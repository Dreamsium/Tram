/* global tabKit cache */
document.getElementById('share-copyLinkTrigger').addEventListener('click', function () {
  const {clipboard} = require('electron')
  clipboard.writeText(cache.get('tabInfo-' + tabKit.currentActiveTab()).URL)
  document.getElementById('share-copyLinkTrigger').getElementsByTagName('p')[0].innerHTML = 'copied!'
  setTimeout(function () {
    document.getElementById('share-copyLinkTrigger').getElementsByTagName('p')[0].innerHTML = 'copy link to clipboard'
  }, 3000)
})
