/* global tabKit cache getComputedStyle */
let uiKit = require('./index.js')
module.exports = function () {
  document.addEventListener('keydown', function (event) {
    if (event.code === 'KeyR' && (event.ctrlKey || event.metaKey)) {
      tabKit.navigation(tabKit.currentActiveTab(), 'reload')
    }
  })

  document.addEventListener('keydown', function (event) {
    if (event.code === 'KeyT' && (event.ctrlKey || event.metaKey) && (event.altKey === false)) {
      tabKit.create(`${__dirname}/../hostedResources/pub/newTabPage/index.html?color=` + getComputedStyle(document.body).getPropertyValue('--main-color').substring(1, 7))
    }
  })

  document.addEventListener('keydown', function (event) {
    if (event.code === 'KeyW' && (event.ctrlKey || event.metaKey)) {
      // code by Bitmapper
      var tabList = cache.get('tabList')
      var tab = tabList.indexOf(tabKit.currentActiveTab())
      if (tabList.length > 1) {
        tabKit.remove(tabKit.currentActiveTab())
        tabKit.changeFocus(cache.get('tabList')[tabKit.getNearestTab(tab)], 'active')
      } else {
        if (tabList.length === 1) {
          tabKit.remove(tabKit.currentActiveTab())
          require('electron').remote.getCurrentWindow().close()
        }
      }
    }
  })

  document.addEventListener('keydown', function (event) {
    if (event.which === 9 && (event.ctrlKey || event.metaKey)) {
      // code from Bitmapper
      var tabList = cache.get('tabList')
      if (tabList.indexOf(tabKit.currentActiveTab()) !== 0) {
        tabKit.changeFocus(cache.get('tabList')[tabList.indexOf(tabKit.currentActiveTab()) - 1], 'active')
      } else if (tabList.length !== tabList.indexOf(tabKit.currentActiveTab()) + 1) {
        tabKit.changeFocus(cache.get('tabList')[tabList.indexOf(tabKit.currentActiveTab()) + 1], 'active')
      }
    }
  })

  document.addEventListener('keydown', function (event) {
    if (event.keyCode === 73 && event.ctrlKey && event.shiftKey) {
      document.getElementById(tabKit.currentActiveTab()).openDevTools()
    }
  })
}

// automatically focus feed pane on ctrl+space
document.addEventListener('keydown', function (event) {
  if (event.keyCode === 32 && (event.ctrlKey || event.metaKey)) {
    uiKit.togglePane('feed')
  }
})

// automatically focus tab pane on ctrl+alt+t
document.addEventListener('keydown', function (event) {
  if (event.keyCode === 84 && (event.ctrlKey || event.metaKey) && (event.altKey)) {
    uiKit.togglePane('tabs')
  }
})

// automatically focus sharing pane on ctrl+alt+s
document.addEventListener('keydown', function (event) {
  if (event.keyCode === 83 && (event.ctrlKey || event.metaKey) && (event.altKey)) {
    uiKit.togglePane('share')
  }
})
