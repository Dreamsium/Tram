/* global cache */
var tabListing = document.getElementById('tabListing')
module.exports = function (action, tabID) {
  switch (action) {
    case 'createdTab':
      var newDiv = document.createElement('div')
      var newP = document.createElement('p')
      newP.appendChild(document.createTextNode(cache.get('tabInfo-' + tabID).title))
      newDiv.appendChild(newP)
      newDiv.classList.add('column')
      newDiv.setAttribute('onclick', "tabKit.changeFocus('" + tabID + "', 'active'); uiKit.togglePane('tabs')")
      newDiv.setAttribute('id', 'forTab-' + tabID)
      tabListing.appendChild(newDiv)
      break

    case 'updateTitle':
      var tabDiv = document.getElementById('forTab-' + tabID)
      tabDiv.getElementsByTagName('p')[0].innerHTML = cache.get('tabInfo-' + tabID).title
      break

    case 'removedTab':
      tabListing.removeChild(document.getElementById('forTab-' + tabID))
      break
  }
}
