/* global cache tabKit */
document.getElementById('settingsWebview').addEventListener('did-navigate', (e) => {
  if (e.url === 'tram-pub://close') {
    require('./index.js').togglePane('settingsPane')
    document.getElementById('settingsWebview').setAttribute('src', `${__dirname}/../hostedResources/prv/settings/index.html`)
  }
})

module.exports = function (paneName) {
  var pane = document.getElementById(paneName)
  var smartField = document.getElementById('smartField')
  switch (pane.classList.contains('activePane')) {
    case false:
      pane.classList.add('activePane')
      var darkener = document.createElement('div')
      darkener.setAttribute('id', 'darkener')
      document.getElementsByTagName('body')[0].appendChild(darkener)
      if (paneName === 'feed') {
        if (cache.get('tabInfo-' + tabKit.currentActiveTab()).URL === undefined || cache.get('tabInfo-' + tabKit.currentActiveTab()).URL.includes('tram-pub://') || cache.get('tabInfo-' + tabKit.currentActiveTab()).URL.includes('tram-prv://') || cache.get('tabInfo-' + tabKit.currentActiveTab()).URL.includes('hostedResources/pub')) {
          smartField.value = ''
        } else {
          smartField.value = cache.get('tabInfo-' + tabKit.currentActiveTab()).URL
        }
        smartField.focus()
        smartField.select()
      }
      break

    case true:
      pane.classList.add('paneAnimateOut')
      pane.classList.remove('activePane')
      document.getElementById('darkener').classList.add('darkenerAnimateOut')
      setTimeout(function () {
        pane.classList.remove('paneAnimateOut')
        document.getElementById('darkener').classList.remove('darkenerAnimateOut')
        document.getElementsByTagName('body')[0].removeChild(document.getElementById('darkener'))
      }, 250)
      if (paneName === 'feed') {
        smartField.blur()
      }
      break
  }
}
