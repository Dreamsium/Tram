document.getElementById('dynamicButton').removeAttribute('onclick')
document.getElementById('dynamicButton').setAttribute('onclick', "window.location.replace('index.html')")

var projInfo = require('../../../projInfo.json')
let authors = projInfo.authors
let source = projInfo.source

console.log(source)

for (var i = 0, len = authors.length; i < len; i++) {
  var newColumn = document.createElement('div')
  newColumn.classList.add('column')
  newColumn.setAttribute('id', 'authorField-' + i)
  newColumn.setAttribute('onclick', "const {clipboard} = require('electron'); clipboard.writeText('" + authors[i].url + "')")
  document.getElementById('contributorsListing').appendChild(newColumn)
  var para = document.createElement('p')
  var node = document.createTextNode(authors[i].name)
  para.appendChild(node)
  document.getElementById('authorField-' + i).appendChild(para)
}

for (var h = 0, lenh = source.length; h < lenh; h++) {
  var newColumnh = document.createElement('div')
  newColumnh.classList.add('column')
  newColumnh.setAttribute('id', 'sourceField-' + h)
  newColumnh.setAttribute('onclick', "const {clipboard} = require('electron'); clipboard.writeText('" + source[h].url + "')")
  document.getElementById('sourcesListing').appendChild(newColumnh)
  var parah = document.createElement('p')
  var nodeh = document.createTextNode(source[h].title)
  parah.appendChild(nodeh)
  document.getElementById('sourceField-' + h).appendChild(parah)
}

var thanksPara = document.createElement('p')
var thanksNode = document.createTextNode(projInfo.thanks)
thanksPara.appendChild(thanksNode)
document.getElementById('overwriteWithThanks').appendChild(thanksPara)

var licenseColumn = document.createElement('div')
licenseColumn.classList.add('column')
licenseColumn.setAttribute('id', 'licenseField')
licenseColumn.setAttribute('onclick', "window.location.replace('agpl.html')")
document.getElementById('licenseContainer').appendChild(licenseColumn)
var licensePara = document.createElement('p')
var licenseNode = document.createTextNode('Click here to view the license.')
licensePara.appendChild(licenseNode)
document.getElementById('licenseField').appendChild(licensePara)
