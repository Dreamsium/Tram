document.getElementById('dynamicButton').removeAttribute('onclick')
document.getElementById('dynamicButton').setAttribute('onclick', "window.location.replace('index.html')")

let autoConfigStore = new Store({name: 'autoConfigStore'})

function resetTram () {
  settingsStore.clear()
  autoConfigStore.clear()
  settingsStore.set('hasBeenSetUp', undefined)
  require('electron').remote.app.relaunch()
  require('electron').remote.app.exit(0)
}
