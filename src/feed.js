/* global tabKit uiKit settingsStore */
// thottery regex for domain name detection
// b/c standard is a moron:
// eslint-disable-next-line
var domainPattern = /^((?:(?:(?:\w[\.\-\+]?)*)\w)+)((?:(?:(?:\w[\.\-\+]?){0,62})\w)+)\.(\w{2,6})$/

var smartField = document.getElementById('smartField')

function redirect (url) {
  // test if it's a valid URL or not
  switch (url.substr(0, 3) !== 'htt') {
    case true:
      url = makeValidURL(smartField.value)
      break
    case false:
      break
  }
  tabKit.navigation(tabKit.currentActiveTab(), 'loadURL', url)
}

// if it has just a domain name and/or a slash, we need to stick a protocol in front of it so the webview can load it
function makeValidURL (url) {
  return 'http://' + url
}

function search (string) {
  redirect(settingsStore.get('searchProvider') + string)
}

function checkIfUrl (string) {
  if (/^https?:\/\//.test(string)) {
    string = /^https?:\/\/(.*)/.exec(string)[1]
  }
  switch (string.indexOf('/') > -1) {
    case true:
      string = string.split('/', 1)
      break
    case false:
      break
  }
  // test if it's a domain name or not
  return domainPattern.test(string)
}

// check for keypress
smartField.addEventListener('keypress', function (e) {
  var key = e.which || e.keyCode
  switch (key) {
    case 13:
      uiKit.togglePane('feed')
      if (smartField.value.substr(0, 11) === 'tram-pub://' || smartField.value.substr(0, 11) === 'tram-prv://') {
        tabKit.navigation(tabKit.currentActiveTab(), 'loadURL', smartField.value)
        return
      }
      switch (checkIfUrl(smartField.value)) {
        case true:
          redirect(smartField.value)
          break
        case false:
          search(smartField.value)
          break
      }
  }
})
