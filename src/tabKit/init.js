/* global tabKit logKit cache getComputedStyle */

let requestingModule = 'init'
let tabID = 'N/A'
let activity = 'Startup'

module.exports = function () {
  // initialize tabList array
  var obj = []
  cache.set('tabList', obj)

  // create first tab
  tabKit.create(`${__dirname}/../hostedResources/pub/newTabPage/index.html?color=` + getComputedStyle(document.body).getPropertyValue('--main-color').substring(1, 7), 'active')
  logKit(requestingModule, tabID, activity, 'info', 'Creating first tab')
}
