/* global tabKit cache */
// code by Bitmapper
module.exports = function (tab) {
  var tabList = cache.get('tabList')
  switch (false) {
    case (tabKit.validityCheck(tabList.indexOf(tab) - 1)):
      return (tabList.indexOf(tab) + 1)
    case (tabKit.validityCheck(tabList.indexOf(tab) + 1)):
      return (tabList.indexOf(tab) - 1)
  }
}
